
# Anime Torrent Downloader

Este proyecto es un script de Python diseñado para automatizar la descarga de torrents de animes desde **UnionFansub** teniendo en cuenta el listado de animes que tengas en _Watching_ en **MyAnimeList** (MAL).

## Características

- **Sincronización con MyAnimeList (MAL)**:
  - Obtiene los animes que estás viendo actualmente desde tu cuenta de MAL usando su API.
  - Refresca automáticamente los tokens de autenticación cuando expiran.
- **Búsqueda y descarga automática en UnionFansub**:
  - Busca títulos de anime en la plataforma **UnionFansub**.
  - Descarga el archivo torrent si el anime está disponible.
- **Manejo de errores**:
  - Manejo de errores de la API de MAL y búsqueda en UnionFansub.
  - Mensajes informativos si no se encuentra un título.

---

## Requisitos previos

1. **Python**: Requiere Python 3.x.
2. **Librerías de Python**:
   - `selenium`
   - `requests`
   - `json`
3. **Google Chrome y ChromeDriver**:
   - Asegúrate de tener instalado Google Chrome.
   - Descarga el [ChromeDriver](https://chromedriver.chromium.org/) correspondiente a tu versión de Chrome y añade su ruta al PATH.
4. **Cuenta en MyAnimeList** y **UnionFansub**:
   - Necesitarás tus credenciales para ambas plataformas.
   - Registra una aplicación en MAL para obtener un `client_id` y configurar el acceso a su API.

---

## Configuración

1. Crea un archivo llamado `config.properties` con el siguiente contenido:

   ```properties
   mal_client_id=TU_CLIENT_ID_DE_MAL
   mal_refresh_token_file=refresh_token.json
   mal_username=TU_NOMBRE_DE_USUARIO_MAL
   unionfansub_username=TU_NOMBRE_DE_USUARIO_UNIONFANSUB
   unionfansub_password=TU_CONTRASEÑA_UNIONFANSUB
   ```

2. Tener el archivo `refresh_token.json` para autenticarte en MAL:

   ```json
   {
     "access_token": "TU_ACCESS_TOKEN",
     "refresh_token": "TU_REFRESH_TOKEN"
   }
   ```

3. Si no deseas usar MAL, puedes crear un archivo llamado `titles.txt` con una lista de títulos de anime, uno por línea.

---

## Uso

1. **Instalar dependencias**:

   Ejecuta el siguiente comando para instalar Selenium:

   ```bash
   pip install selenium
   ```

   Si no tienes el módulo `requests`, instálalo con:

   ```bash
   pip install requests
   ```

2. **Ejecutar el script**:

   ```bash
   python main.py
   ```

   El script:
   - Sincronizará tu lista de MAL o leerá los títulos desde `titles.txt`.
   - Buscará y descargará torrents de animes desde UnionFansub.

---

## Estructura del proyecto

- `main.py`: Script principal.
- `config.properties`: Configuración con credenciales y claves.
- `refresh_token.json`: Tokens de autenticación para MAL.
- `titles.txt`: Lista de títulos de anime (opcional).

---

## Notas importantes

1. **Tokens de MAL**:
   - Los tokens expiran después de un tiempo, pero el script intenta refrescarlos automáticamente.
2. **Manejo de errores**:
   - Si no se encuentra un anime en UnionFansub, se mostrará un mensaje en consola.
   - Si la API de MAL no responde, el script intentará leer los títulos desde `titles.txt`.
3. **Tiempos de espera**:
   - El script incluye retardos (`time.sleep`) para garantizar que las páginas se carguen correctamente.

---

## Contribuciones

¡Se aceptan contribuciones! Si tienes ideas o encuentras problemas, crea un **issue** o envía un **pull request**.

---

## Licencia

Este proyecto se distribuye bajo la Licencia MIT. Consulta el archivo `LICENSE` para más detalles.
