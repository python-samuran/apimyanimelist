import os
import requests
import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time

def load_config(filename):
    config = {}
    with open(filename, 'r') as f:
        for line in f:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=')
                config[key.strip()] = value.strip()
    return config

def refresh_token(client_id, refresh_token_file):
    with open(refresh_token_file) as f:
        data = json.load(f)

    url='https://myanimelist.net/v1/oauth2/token'
    refresh_token=data['refresh_token']

    body={'client_id': client_id, 'grant_type': 'refresh_token', 'refresh_token': refresh_token}

    response=requests.post(url, data=body)

    if response.status_code == 200:
        with open("access_token.json", "w") as f:
            f.write(response.text)
        return True
    else:
        print(f"Fallo para refrescar el token: {response.status_code}")
        return False

def get_animelist(client_id, refresh_token_file, mal_username):
    with open(refresh_token_file) as f:
        data = json.load(f)

    bearer_token = data['access_token']
    headers = {"Authorization": f"Bearer {bearer_token}"}
    
    url = f"https://api.myanimelist.net/v2/users/{mal_username}/animelist?fields=list_status&status=watching"
    
    response = requests.get(url, headers=headers)
    
    return response

def read_titles_from_file(filename):
    with open(filename, "r") as f:
        return [line.strip() for line in f]
    
def download_torrent(driver, torrent_id):
    download_url = f"https://torrent.unionfansub.com/download.php?torrent={torrent_id}&aviso=1"
    driver.get(download_url)
    time.sleep(5)

titles = []

config = load_config('config.properties')

if os.path.exists('titles.txt'):
    print("El fichero titles.txt existe, buscando de local...")
    titles = read_titles_from_file('titles.txt')
else:

    try:
        response = get_animelist(config['mal_client_id'], config['mal_refresh_token_file'], config['mal_username'])
        response.raise_for_status()
        data=response.json()
        titles = [item['node']['title'] for item in data['data']]                
        for item in data['data']:
            print(item['node']['title'])
        with open('titles.txt', 'w') as f:
            for title in titles:
                f.write(f"{title}\n")

    except requests.exceptions.HTTPError as http_err:
        if response.status_code == 401:
            print("Token expirado. Refrescando token...")
            if refresh_token(config['mal_client_id'], config['mal_refresh_token_file']):
                response = get_animelist(config['mal_client_id'], config['mal_refresh_token_file'], config['mal_username'])
                if response.status_code==200:
                    data=response.json()
                    for item in data['data']:
                        print(item['node']['title'])
                    titles = [item['node']['title'] for item in data['data']]
                    with open('titles.txt', 'w') as f:
                        for title in titles:
                            f.write(f"{title}\n")               
                else:
                    print(f"Fallo al obtener el listado después de obtener el listado: {response.status_code}")
                    titles = []
            else:
                print("Fallo al refrescar el token.")
                titles = []
        elif response.status_code == 504:
            print("El API ha devuelto 504. Leyendo titulos desde local...")
            titles = read_titles_from_file('titles.txt')
        else:
            print(f"HTTP error: {http_err}")
            titles = []
    except Exception as err:
        print(f"Otro error: {err}")
        titles = []

if titles:

    driver = webdriver.Chrome()
    
    driver.get("https://torrent.unionfansub.com/login.php")
    
    unionfansub_username = config['unionfansub_username']
    unionfansub_password = config['unionfansub_password']
    
    username_input = driver.find_element(By.ID,"quick_login_username")
    password_input = driver.find_element(By.ID,"quick_login_password")

    username_input.send_keys(unionfansub_username)
    password_input.send_keys(unionfansub_password)
    
    password_input.send_keys(Keys.RETURN)
    
    time.sleep(5)
    
    driver.get("https://torrent.unionfansub.com/browse.php")

    for title in titles:
        search_box = driver.find_element(By.NAME, "search")
        search_box.clear()
        search_box.send_keys(title)
        search_box.send_keys(Keys.RETURN)

        time.sleep(3)

        try:

            first_result = driver.find_element(By.XPATH, "/html/body/div[4]/div/table/tbody/tr[2]/td[2]/div/a")
            torrent_url = first_result.get_attribute('href')

            torrent_id = torrent_url.split('id=')[1].split('&')[0]

            download_torrent(driver, torrent_id)

        except NoSuchElementException:
            # Manejo de la excepción cuando no hay resultados
            print(f"No se encontró ningún resultado para el título: {title}")
    
    driver.quit()
